const BOOM = require('express-boom');

const AUTHENTICATION_MODEL = require('./data.model');
const API_CONFIG = require('../env/api.config');
const COMMON = require('../common/common');

const READ_PREFERENCE = require('mongodb').ReadPreference;

// Ensure that DB is connected
require('../env/db.config').connectDB(API_CONFIG['dbUri']);

// Method for getting authentication(s)
function getAuthentication(request, response) {

}

// Method for posting new authentication
function postAuthentication(request, response) {
    // IMPORTANT 
    // Code below is a mockup intented to simulate correct behavior on web client.
    // Thus no DB access is made and token is fixed (with secret yebba)
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJuYW1lIjoiSXp0b2sgQmVyZG5payIsInByb2Nlc3MiOlt7ImlkIjoieWItcDEiLCJyb2xlSWQiOiJ5Yi1wMS1yMSJ9XX0.nBaA3bqRCo8sPEjcM28jNS2iseuedUsJNbQKTewRCQc';
    let username = '';
    let password = '';

    // console.log('Username:', request.body.username);
    // console.log('Password:', request.body.password);

    if (request.body.username) {
        username = typeof (request.body.username) !== 'undefined' ? request.body.username : '';
        
    }
    
    if (request.body.password) {
        password = typeof (request.body.password) !== 'undefined' ? request.body.password : '';
        
    }

    if ((username === 'iztok.berdnik@gmail.com') && (password === 'Yebba')) {
        response.send(token);
    } else {
        response.status(400).send('Wrong username or password');
    }
}

// Method for updating authentication
function putAuthentication(request, response) {

}

// Method for deleting authentication
function deletedAuthentication(request, response) {

}

module.exports = {
    getAuthentication,
    postAuthentication,
    putAuthentication,
    deletedAuthentication
};
