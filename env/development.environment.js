// DB settings
const dbPort = 23933;
const dbName = 'yebba_core';
const dbUsername = 'yb_robot';
const dbPassword = 'frog4ever';

// API settings
const apiPort = 3005;

// Export settings as module
module.exports = {
    dbPort,
    dbName,
    dbUsername,
    dbPassword,
    apiPort
};