const EXPRESS = require('express');
const API_ROUTER = EXPRESS.Router();
const LANGUAGE_SERVICE = require('../services/language.service');
const TRANSLATION_SERVICE = require('../services/translation.service');
const USERTYPE_SERVICE = require('../services/user.service');
const THEME_SERVICE = require('../services/theme.service');
const MODULE_SERVICE = require('../services/module.service');
const APPLICATION_SETTINGS_SERVICE = require('../services/applicationSettings.service');
const AUTHENTICATION_SERVICE = require('../services/authenticate.service');

// Default route definition
API_ROUTER.route('/api')
    .get(function(req, res, next) {
    response.send('API works. Consult API docs for specific routes.');
});


// ** Application Settings API **
API_ROUTER.route('/api/applicationSettings')
    .get(function(request, response, next) {
    
    // Pass request to application settings service
    APPLICATION_SETTINGS_SERVICE.getApplicationSettings(request, response);
});

// ** User authentication API **
API_ROUTER.route('/api/authenticate')
    .get(function(request, response, next) {
        response.status(400).json('Not supported');
    })
    .post(function(request, response, next) {
    // console.log(request.body);
    // Pass request to application settings service
    AUTHENTICATION_SERVICE.postAuthentication(request, response);
});

// ** Languages API **
API_ROUTER.route('/api/languages')
    .get(function(request, response, next) {

    // Pass request to service.
    LANGUAGE_SERVICE.getLanguages(request, response);
}); 

// ** Translations API **
API_ROUTER.route('/api/locale/:id')
    .get(function(request, response, next) {

    // Pass request to service.
    TRANSLATION_SERVICE.getTranslation(request, response);
});

// ** Modules API **
API_ROUTER.route('/api/modules/:id') 
    .get(function(request, response, next) {
    // Pass request to service
    MODULE_SERVICE.getModules(request, response);
});

// ** User API **
API_ROUTER.route('/api/usertype/:id')
    .get(function(request, response, next) {
    // Pass request to service
    USERTYPE_SERVICE.getUserType(request, response);
});

// ** Theme API **
API_ROUTER.route('/api/themes/:id')
    .get(function(request, response, next) {
    // Pass request to service
    THEME_SERVICE.getThemes(request, response);
});

// Unknown route response. Note that wildcard route definition must be defined as a last item. 
// Otherwise all request will default to this route.
API_ROUTER.route('*')
    .get(function(request, response, next) {
    response.send('No idea what are you looking for!');
});

// Export routes
module.exports = API_ROUTER;