// Framework
const EXPRESS = require('express');
const PATH = require('path');
const BODY_PARSER = require('body-parser');
const BOOM = require('express-boom');
const API_ROUTER = EXPRESS.Router();

// Application
const API_CONFIG = require('./env/api.config');
const DB_CONFIG = require('./env/db.config');
const API_ROUTES = require('./routes/routes');

// DB connection object
let DB_CONNECTION = null;

/* // API application object
const yebbaCoreApi = EXPRESS();

yebbaCoreApi.use('', API_ROUTES);

// Register middleware
yebbaCoreApi.use(BOOM());
yebbaCoreApi.use(BODY_PARSER.json());
yebbaCoreApi.use(BODY_PARSER.urlencoded({
    extended: true
})); */

// API server class
class yebbaCoreApiServer {

    constructor() {
        // Start up DB connection
        const _DB_URI = API_CONFIG['dbUri'];
        this.startDBConnection(_DB_URI);
    }

    // Server start method
    startAPI() {
        // API application object
        const yebbaCoreApi = EXPRESS();

        // Register middleware
        yebbaCoreApi.use(BOOM());
        yebbaCoreApi.use(BODY_PARSER.json());
        yebbaCoreApi.use(BODY_PARSER.urlencoded({
            extended: true
        }));
        yebbaCoreApi.use(API_ROUTES);

        yebbaCoreApi.listen(
            API_CONFIG['apiPort'], () => {
                console.log('Yebba Core API is running on localhost:' + API_CONFIG['apiPort']);
            }
        );
    }

    // Start DB connection method
    // Args:
    // [0] :: Database URI
    startDBConnection(dbURI) {
        DB_CONFIG.connectDB(dbURI);

        const _DB_CONNECTION = DB_CONFIG['mongoose'].connection;
        
        // Set connection object to global scope
        DB_CONNECTION = _DB_CONNECTION;

        // Error event after connection was established.
        _DB_CONNECTION.on('error', () => {
            console.error('MongoDB Connection Error. Please make sure that ' + dbURI + ' is running.');
        });

        // On connection open event
        _DB_CONNECTION.once('open', () => {
            console.log(':: Connected to DB ::', dbURI);
        });

        // Only start-up API if connection to DB was successfully established.
        this.startAPI();
    }
}

// Fire-up API server object
const yebbabCoreApiServer = new yebbaCoreApiServer();
