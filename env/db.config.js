const mongoose = require('mongoose');


// Set to Node.js native promises as specified here http://mongoosejs.com/docs/promises.html
mongoose.Promise = global.Promise;


// Method for connection to DB
// Args:
// [0] :: Database URI
function connectDB(URI) {
    mongoose.set('debug', true);
    console.log(':: Connecting to DB ::');
    return mongoose.connect(
        URI,
        {
            useMongoClient: true
        }
    ).then(
        () => {

        },
        err => {
            // Handle initial error
            console.error('\x1b[31m', 'MongoDB Connection Error. Please make sure that ' + URI + ' is running.');
            console.log('\x1b[0m');
        }
    );
}

// Export module
module.exports = {
    mongoose,
    connectDB
};