// CONFIG for application core service

// Application mode. It is fixed to DEVELOPMENT. This property will be set during build process.
const APPLICATION_MODE = 'DEVELOPMENT';
const APPLICATION_SETTINGS = require('./development.environment');

// Read appropriate environement properties
if (APPLICATION_MODE === 'DEVELOPMENT') {
    console.log(':: Starting server in DEVELOPMENT mode ::');
    const APPLICATION_SETTINGS = require('./development.environment');
}
else {
    console.log(':: Starting server in PRODUCTION mode ::');
    const APPLICATION_SETTINGS = require('./production.environment');
}

// API root
const API_ROOT = './';

// Allow lint exception
// eslint-disable-next-line max-len
const DB_URI = 'mongodb://' + APPLICATION_SETTINGS['dbUsername'] + ':' + APPLICATION_SETTINGS['dbPassword'] + '@ds123933.mlab.com:' + APPLICATION_SETTINGS['dbPort'] + '/' + APPLICATION_SETTINGS['dbName'];

module.exports = {
    applicationMode: APPLICATION_MODE,
    dbUri: DB_URI,
    apiRoot: API_ROOT,
    apiPort: APPLICATION_SETTINGS['apiPort']
};
