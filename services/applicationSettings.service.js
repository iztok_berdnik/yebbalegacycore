const BOOM = require('express-boom');

const MODULE_MODEL = require('./data.model');
const API_CONFIG = require('../env/api.config');
const COMMON = require('../common/common');

const READ_PREFERENCE = require('mongodb').ReadPreference;


// Ensure that DB is connected
require('../env/db.config').connectDB(API_CONFIG['dbUri']);

// Method for getting the list of locale translations
function getApplicationSettings(request, response) {
    // List of user types. For now its plain JSON file. In the future, to be inserted into DB.

    // Resolve referer
    var referer = request.get('Referer');

    var application = '';
    
    // Applications are separated by port in development mode. In reallty it would be domain names
    // This code should be adapted to that case in the future
    if (referer.indexOf('4250') > 0) {
        application = '4250';
    } else if (referer.indexOf('4251') > 0) {
        application = '4251';
    } else {
        application = '4250';
    }
    
    const aplicationSettings = 'applicationSettings-' + application + '.json';

    const localeFile = require('../data/applicationSettings/' + aplicationSettings);

    response.send(localeFile);
}

// Method for posting new locale translation
function postApplicationSettings(request, response) {
    
}

// Method for updating locale translations
function putApplicationSettings(request, response) {

}

// Method for deleting locale translations
function deletedApplicationSettings(request, response) {

}

module.exports = {
    getApplicationSettings,
    postApplicationSettings,
    putApplicationSettings,
    deletedApplicationSettings
};