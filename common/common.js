

// Method for checking error
function checkServerError(response, error) {
    if (error) {
        response.status(500).send(error);
    }
}

// Export module
module.exports = {
    checkServerError
};