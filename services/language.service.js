const BOOM = require('express-boom');

const LANGUAGE_MODEL = require('./data.model');
const API_CONFIG = require('../env/api.config');
const COMMON = require('../common/common');

const READ_PREFERENCE = require('mongodb').ReadPreference;

// Ensure that DB is connected
require('../env/db.config').connectDB(API_CONFIG['dbUri']);

// Method for getting the list of languages
function getLanguages(request, response) {

    // List of languages. For now its plain JSON file. In the future, to be inserted into DB.
    const languageList = require('../data/languages/languages.json');
    // response.statusCode = 400;
    // response.send('This is test error');
    response.send(languageList);
}

// Method for posting new language
function postLanguage(request, response) {

}

// Method for updating language
function putLanguage(request, response) {

}

// Method for deleting language
function deletedLanguage(request, response) {

}

module.exports = {
    getLanguages,
    postLanguage,
    putLanguage,
    deletedLanguage
};