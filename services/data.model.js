const MONGOOSE = require('mongoose');

const SCHEMA = MONGOOSE.Schema;

const ILanguage = new SCHEMA({
    id: {
        type: Number, 
        required: true,
        unique: true
    },
    code: {
        type: String,
        required: true
    },
    native: {
        type: String,
        required: true
    },
    global: {
        type: String,
        required: true
    }
});

const ITranslation = new SCHEMA({
  id: {
      type: Number,
      required: true
  },
  value: {
      type: String,
      required: true 
    }
});

const IUserType = new SCHEMA({
    id: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        required: true
    }
});

const IModule = new SCHEMA({
    id: {
        type: Number,
        required: true
    },
    moduleName: {
        type: String,
        required: true
    },
    moduleDescription: {
        type: String,
        required: true
    },
    moduleIcon: {
        type: String,
        required: true
    },
    moduleUrl: {
        type: String,
        required: true
    }
});

const IApplicationSettings = new SCHEMA({
    applicationTheme: {
        type: String,
        required: true
    },
    customer: {
        type: String,
        required: true
    },
    customerLogo: {
        type: String,
        required: true
    },
    licensedTo: {
        type: String,
        required: true
    },
    licenseeLogo: {
        type: String,
        required: true
    }
});

// Language model
const LANGUAGE = MONGOOSE.model('Language', ILanguage);

// Translation model
const TRANSLATION = MONGOOSE.model('Translation', ITranslation);

// User type model
const USERTYPE = MONGOOSE.model('UserType', IUserType);

// Modules model
const MODULE = MONGOOSE.model('Module', IModule);

// ApplicationSettings model
const APPLICATION_SETTINGS = MONGOOSE.model('ApplicationSettings', IApplicationSettings);

module.exports = {
    LANGUAGE,
    TRANSLATION,
    USERTYPE,
    MODULE,
    APPLICATION_SETTINGS
};