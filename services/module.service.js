const BOOM = require('express-boom');

const MODULE_MODEL = require('./data.model');
const API_CONFIG = require('../env/api.config');
const COMMON = require('../common/common');

const READ_PREFERENCE = require('mongodb').ReadPreference;

// Ensure that DB is connected
require('../env/db.config').connectDB(API_CONFIG['dbUri']);

// Method for getting the list of locale translations
function getModules(request, response) {
    // List of user types. For now its plain JSON file. In the future, to be inserted into DB.

    // Resolve requested locale
    const _locale = request.params.id;

    if (_locale.length != 5) {
        console.warn('\x1b[33m', ':: INVALID REQUEST in getModules method ::', _locale);
        console.log('\x1b[0m');
        // response.statusCode = 400;
        // response.send('Couldn´t resolve locale translation you are looking for. Did you include language code (e.g. en-US)?');
        console.log((BOOM()));
        response.send(BOOM());
        //BOOM.badRequest('Couldn´t resolve locale translation you are looking for. Did you include language code (e.g. en-US)?')

        return;
    }

    const _localList = 'modules.' + _locale + '.json';

    const localeFile = require('../data/modules/' + _localList);

    response.send(localeFile);
}

// Method for posting new locale translation
function postModule(request, response) {
    
}

// Method for updating locale translations
function putModule(request, response) {

}

// Method for deleting locale translations
function deletedModule(request, response) {

}

module.exports = {
    getModules,
    postModule,
    putModule,
    deletedModule
};